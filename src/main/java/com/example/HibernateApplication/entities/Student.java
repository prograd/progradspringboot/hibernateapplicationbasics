package com.example.HibernateApplication.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student_table")
public class Student {
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "sname",length = 20)
    private String sname;
    private  int rollno;
    private int std;

    public Student() {
    }

    public Student(int id, String sname, int rollno, int std) {
        this.id = id;
        this.sname = sname;
        this.rollno = rollno;
        this.std = std;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getRollno() {
        return rollno;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }

    public int getStd() {
        return std;
    }

    public void setStd(int std) {
        this.std = std;
    }
}
