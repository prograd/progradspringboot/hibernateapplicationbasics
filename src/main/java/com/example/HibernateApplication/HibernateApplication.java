package com.example.HibernateApplication;

import com.example.HibernateApplication.entities.Student;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class HibernateApplication {

	public static void main(String[] args) {

		//SpringApplication.run(HibernateApplication.class, args);

		//String is used to store the configurationFile name along with extension
//        String ConfigurationFile="hibernate.cfg.xml";
//		//below lines are used to fetch the session factory from resources folder and
//		// get the session factory object in the class where u want to load it
//        ClassLoader classLoaderObj=HibernateApplication.class.getClassLoader();
//		File f=new File(classLoaderObj.getResource(ConfigurationFile).getFile());
//		//the session factory class object from configuration file is fetched
//		// and assigned to session factory object
//		SessionFactory sessionFactoryObj=new Configuration().configure(f).buildSessionFactory();
//		Session sessionObj=sessionFactoryObj.openSession();
//        savRecord(sessionObj);

		sessionFactory=getSessionFactory();
				Session sessionObj=sessionFactory.openSession();
//             savRecord(sessionObj);
 			 update(sessionObj);
			// delete(sessionObj);
			 display(sessionObj);
	}
	private static SessionFactory sessionFactory;
	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/amar");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "mysql");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

				settings.put(Environment.SHOW_SQL, "true");

				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

				settings.put(Environment.HBM2DDL_AUTO, "update");

				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Student.class);//we have to add all class that is table in db

				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();

				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
	public static void display(Session sessionObj){
		Query query=sessionObj.createQuery("FROM Student");
		List<Student> list=query.list();
		for (Student obj:list) {
			System.out.println(obj.getRollno()+" "+obj.getSname()+" "+obj.getStd());
		}
	}
	public static void update(Session sessionObj){
		int rollno=10;
		Student studentObj= (Student) sessionObj.get(Student.class,rollno);
	    studentObj.setSname("DEEPAK");
		studentObj.setStd(12);
		sessionObj.beginTransaction();
		sessionObj.saveOrUpdate(studentObj);
		sessionObj.getTransaction().commit();
		System.out.println("record updated");

	}
	public static void delete(Session sessionObj){
		int rollno=10;
	 	Student studentObj= (Student) sessionObj.get(Student.class,rollno);
	    sessionObj.beginTransaction();
		sessionObj.delete(studentObj);
	    sessionObj.getTransaction().commit();
		System.out.println("record deleted");
	}
	private static void savRecord(Session sessionObj) {
		Student s = new Student();
		s.setSname("dada");
		s.setId(3);
		s.setRollno(11);
		s.setStd(18);
		sessionObj.beginTransaction();
		sessionObj.save(s);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");
	}
}
